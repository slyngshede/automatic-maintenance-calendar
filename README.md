# Automatic Maintenance Calendar
aka "Voices of Doom"


## Getting started

```
$ pip -mvenv .venv
$ pip install -r requirements.txt
$ python -m amc.__init__ --config /etc/amc/test.ini
```
