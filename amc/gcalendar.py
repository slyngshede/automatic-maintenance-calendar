# SPDX-License-Identifier: GPL-3.0-or-later
import json
import os
import uuid

from datetime import datetime

from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials


class Calendar():
    SCOPES = ['https://www.googleapis.com/auth/calendar']

    def __init__(self, calendar:str, token_path: str='token.json'):
        self.calendar_id = f'{calendar}@group.calendar.google.com'
        self.token: str = token_path
        self.creds: Credentials = self._load_credentials(token_path)
        self.service = build('calendar', 'v3', credentials=self.creds)

    def _load_credentials(self, path) -> Credentials:
        if os.path.exists(path):
            with open(self.token) as source:
                info = json.load(source)
                creds = service_account.Credentials.from_service_account_info(info, scopes=self.SCOPES)
        else:
            raise FileExistsError(f'unable to locate credentials: {path}')
        return creds

    def refresh_credentials(self):
        if not self.creds or not self.credscreds.valid:
            self.creds = self._load_credentials(self.token)

        if self.creds and self.creds.expired and self.creds.refresh_token:
            self.creds.refresh(Request())

    def get_event(self, id: str):
        event = self.service.events().get(calendarId=self.calendar_id, eventId=id).execute()
        return event

    def get_event_ical(self, ical_id: str):
        e = []
        page_token = None

        while True:
            events = self.service.events().list(calendarId=self.calendar_id, iCalUID=ical_id, pageToken=page_token).execute()
            e.extend(events['items'])
            page_token = events.get('nextPageToken')
            if not page_token:
                break
        if not e:
            return None
        return e[0]


    def get_events(self):
        e = []
        page_token = None

        while True:
            events = self.service.events().list(calendarId=self.calendar_id, pageToken=page_token).execute()
            e.extend(events['items'])
            page_token = events.get('nextPageToken')
            if not page_token:
                break
        return e

    def create_event(self, ical_id: str, summary: str, start: datetime, end: datetime, description: str=""):
        event = self.get_event_ical(ical_id)
        if event:
            return event

        timezone = start.tzname()
        event_data = {
            'iCalUID': ical_id,
            'summary': summary,
            'description': description,
            'start': {
                'dateTime': start.isoformat(),
                'timeZone': timezone
            },
            'end': {
                'dateTime': end.isoformat(),
                'timeZone': timezone
            }
        }
        return self.service.events().insert(calendarId=self.calendar_id, body=event_data).execute()


if __name__ == "__main__":
    c = Calendar('c_d9faf17ed3c70061f008197528a5e7246f8167ba196633d9239ce6ca16c69d65@group.calendar.google.com')

    start = datetime.strptime('2023-06-10 00:00:00+0000', '%Y-%m-%d %H:%M:%S%z')
    end = datetime.strptime('2023-06-11 00:00:00+0000', '%Y-%m-%d %H:%M:%S%z')
    id = f'{uuid.uuid4().hex}@wikimedia.org'
    id = '0672ec31e97d40b1a724d141ac82c9e2@wikimedia.org'
    print(id)
    c.create_event(id, 'Birthday', start, end)
    event = c.get_event_ical('hest')
    print(event)
    print("========================")
    event = c.get_event_ical(id)
    print(event)