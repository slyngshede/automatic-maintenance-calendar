# SPDX-License-Identifier: GPL-3.0-or-later
import mailbox


def read_messages(config):
    path = config['mbox']['path']
    limit = config['mbox'].get('limit', 5000)

    mbox = mailbox.mbox(path)
    messages = []
    for msg in mbox:
        messages.append(msg)
        if len(messages) == limit:
            break

    return messages