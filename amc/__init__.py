# SPDX-License-Identifier: GPL-3.0-or-later
import argparse
import configparser
import importlib

from datetime import datetime
from mailbox import Message
from pathlib import Path

import pytz

from circuit_maintenance_parser import (init_provider,
                                        Maintenance,
                                        NotificationData)

from .gcalendar import Calendar


DEFAULT_CONFIG_PATH='/etc/amc/config.ini'
PROVIDERS = {
    'lumen': 'lumen',
    'equinix': 'equinix',
    'eunetworks': 'eunetworks',
    'ntt': 'ntt',
    'gtt': 'gtt',
    'apple': 'apple'
}

def get_provider(msg):
    # find custom provider, if required.
    for k,v in PROVIDERS.items():
        try:
            if k in msg['subject'].lower():
                return init_provider(v)
            elif k in msg['X-Original-Sender'].lower():
                return init_provider(v)
        except:
            pass

    # assume best case parser and handle errors later.
    return init_provider()

def text_from_mail_message(msgs: Message) -> str:
    for part in msgs:
        content_type = part.get_content_type()
        if content_type == 'multipart/alternative':
            return text_from_mail_message(part.get_payload())

        if content_type == 'text/plain':
            return part.get_payload()

    return ''


def maintenance_to_event(m: Maintenance, msg: Message, cal: Calendar):
    description = text_from_mail_message(msg.get_payload())
    uid = m.uid
    if uid == '0':
        uid = m.maintenance_id + m.circuits[0].circuit_id
    event = cal.create_event(uid,
                             msg['subject'],
                             start=datetime.fromtimestamp(m.start, pytz.utc),
                             end=datetime.fromtimestamp(m.end, pytz.utc),
                             description=description if description else m.summary
                             )
    return event


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Automatically import vendor maintenance windows into a shared Google calendar")
    parser.add_argument('--config', default=DEFAULT_CONFIG_PATH)
    args = parser.parse_args()

    path = Path(args.config)
    if not path.exists():
        print("Configuration file not found: {path}")
        exit(1)

    config = configparser.ConfigParser()
    config.read(path)

    calendar_id = config['DEFAULT']['calendar']
    token_path = config['DEFAULT'].get('token', path.parent.joinpath('token.json'))
    source = config['DEFAULT'].get('source', None)

    if not source:
        print("No message source provider configured")
        exit(1)

    calendar = Calendar(calendar_id, token_path)
    msg_src = importlib.import_module(f'.{source}', 'amc')

    for msg in msg_src.read_messages(config):
        if msg['from'] == 'sre-observability@wikimedia.org':
            continue

        provider = get_provider(msg)
        data = NotificationData.init_from_emailmessage(msg)
        try:
            for maintenance in provider.get_maintenances(data):
                maintenance_to_event(maintenance, msg, calendar)
        except Exception as e:
            print(e)
            pass
